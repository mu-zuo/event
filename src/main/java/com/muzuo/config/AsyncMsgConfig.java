package com.muzuo.config;

import java.lang.reflect.Field;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import com.alibaba.ttl.threadpool.TtlExecutors;
import com.muzuo.event.AbstractBaseMsg;
import com.muzuo.event.AsyncMsgExceptionHandler;
import com.muzuo.event.BaseEvent;


/**
 * 异步消息处理配置
 * @ClassName: AsyncMsgConfig
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2024年3月16日
 *
 */
@Configuration
public class AsyncMsgConfig implements AsyncConfigurer {
    @Autowired
    ApplicationContext ctx;
    
    @Bean(name= Constants.MSG_HANDLER_EXECUTOR_NAME)
    @Override
    public Executor getAsyncExecutor() {
        ScheduledThreadPoolExecutor executor = new MyScheduledThreadPoolExecutor(2);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
        executor.setThreadFactory(new CustomizableThreadFactory("msg-handler-thread-"));
        return TtlExecutors.getTtlScheduledExecutorService(executor);

    }
    /**
     * 自定义线程池 延时队列 默认不延时 交给消息延迟
     * @ClassName: MyScheduledThreadPoolExecutor
     * @Description: TODO(这里用一句话描述这个类的作用)
     * @date 2024年3月13日
     *
     */
    public class MyScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor{
        public MyScheduledThreadPoolExecutor(int corePoolSize) {
            super(corePoolSize);
        }
        @Override
        public <T> Future<T> submit(Callable<T> task) {
            long delay = findDelaySeconds(task);
            return schedule(task, delay, TimeUnit.SECONDS);
        }

        private <T> long findDelaySeconds(Callable<T> task){
            long delay = 0;
            try {
                Field callable = task.getClass().getDeclaredField("callable");
                callable.setAccessible(true);
                Object callableValue = callable.get(task);
                Field field = callableValue.getClass().getDeclaredField("arg$2");
                field.setAccessible(true);
                Object value = field.get(callableValue);
                if (value instanceof MethodInvocation) {
                    MethodInvocation invocation = (MethodInvocation) value;
                    Object[] args = invocation.getArguments();
                    if (args != null && args[0] != null && args[0] instanceof BaseEvent<?>) {
                        @SuppressWarnings("unchecked")
                        BaseEvent<AbstractBaseMsg> msg = (BaseEvent<AbstractBaseMsg>) args[0];
                        delay = msg.getData().getDelaySeconds();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return delay;
        }
    }
    
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return ctx.getBean(AsyncMsgExceptionHandler.class);
    }
}
