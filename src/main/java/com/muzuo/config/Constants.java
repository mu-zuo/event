package com.muzuo.config;

public interface Constants {
    /** 异步处理消息线程池 */
    String MSG_HANDLER_EXECUTOR_NAME = "msgHandlerExecutor";
}
