package com.muzuo.event;

public class CustomerRegister extends AbstractBaseMsg{

    private static final long serialVersionUID = 1L;
    private String userId;
    public CustomerRegister() {
        super(CustomerMessageType.REGISTER_SUCCESS_MSG);
    }

    public static CustomerRegister build(String userId) {
        CustomerRegister register = new CustomerRegister();
        register.setUserId(userId);
        register.setDelayLevel(DelayLevelType.Delay_5s);
        return register;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    
}
