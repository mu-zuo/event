package com.muzuo.event;

/**
 * 客户类消息
 * @ClassName: CustomerMessageType
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2024年2月22日
 *
 */
public enum CustomerMessageType implements IMessageType{
    /**
     * 注册成功
     */
    REGISTER_SUCCESS_MSG("REGISTER_SUCCESS_MSG"),
    
    ;

    CustomerMessageType(String tag) {
        this.topic=EventTopicNameType.ESB_CUSTOMER_TOPIC;
        this.tag = tag;
    }
    private EventTopicNameType topic;
    private final String tag;
    @Override
    public String getTopic() {
        return topic.name();
    }
    @Override
    public String getTag() {
        return tag;
    }
    
    public void setTopic(EventTopicNameType topic) {
        this.topic = topic;
    }
    public static CustomerMessageType fromString(String tags) {
        for (CustomerMessageType type : CustomerMessageType.values()) {
            if (type.getTag().equals(tags)) {
                return type;
            }
        }
        return null;
    }
}
