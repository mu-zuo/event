package com.muzuo.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import com.muzuo.config.Constants;

@Async(Constants.MSG_HANDLER_EXECUTOR_NAME)
@Component
public class AsyncEventListener {
    private static final Logger logger = LoggerFactory.getLogger(AsyncEventListener.class);
    @EventListener
    public void handleCustomerRegister(BaseEvent<CustomerRegister> event){
        CustomerRegister data = event.getData();
        logger.info("**************接收到消息CustomerRegister"+data.getUserId()+"*********");
    }
}