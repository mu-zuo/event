package com.muzuo.event;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
/**
 * 事件内容抽象基类
 * @ClassName: AbstractBaseMsg
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2024年2月22日
 *
 */
public abstract class AbstractBaseMsg implements Serializable {

    private static final long serialVersionUID = 1L;
    /** 事件生成时间 */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createMsgTime;
    private IMessageType messageType;
    private DelayLevelType delayLevel;
    
    public AbstractBaseMsg(IMessageType messageType) {
        this.createMsgTime = LocalDateTime.now();
        this.messageType = messageType;
    }
    public LocalDateTime getCreateMsgTime() {
        return createMsgTime;
    }
    public void setCreateMsgTime(LocalDateTime createMsgTime) {
        this.createMsgTime = createMsgTime;
    }
    public IMessageType getMessageType() {
        return messageType;
    }
    public void setMessageType(IMessageType messageType) {
        this.messageType = messageType;
    }
    public DelayLevelType getDelayLevel() {
        return delayLevel;
    }
    public void setDelayLevel(DelayLevelType delayLevel) {
        this.delayLevel = delayLevel;
    }
    /**
     * 获取延时秒数
     * @Title: getDelaySeconds
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @return    参数
     * @return long    返回类型
     */
    public long getDelaySeconds(){
        if (delayLevel != null) {
            return DelayLevelType.getLevelSeconds(delayLevel);
        }
        return 0;
    }
    
}
