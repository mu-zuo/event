package com.muzuo.event;

import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;
/**
 * spring发送消息内容
 * @ClassName: BaseEvent
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2024年2月22日
 * @param <T>
 */
public class BaseEvent<T extends AbstractBaseMsg> implements ResolvableTypeProvider{
    /** 具体时间消息内容 */
    private T data;
    public BaseEvent(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public ResolvableType getResolvableType() {
        return ResolvableType.forClassWithGenerics(getClass(), ResolvableType.forInstance(getData()));
    }

}
