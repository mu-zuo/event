package com.muzuo.event;

/**
 * 消息类型
 * @ClassName: IMessageType
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2021年1月12日
 *
 */
public interface IMessageType {
    /**
     * 获取topic名称
     * @Title: getTopic
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @return    参数
     * @return String    返回类型
     */
    String getTopic();
    /**
     * 获取消息topic的tag标签
     * @Title: getTag
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @return    参数
     * @return String    返回类型
     */
    String getTag();
}
