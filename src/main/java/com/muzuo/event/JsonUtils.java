package com.muzuo.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.TimeZone;

public class JsonUtils {
    private static class Instance{
        public static JsonUtils instance = new JsonUtils();
    }

    private ObjectMapper objectMapper = new ObjectMapper();

    private JsonUtils(){
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_ABSENT)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .setTimeZone(TimeZone.getDefault())
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
                .disable(SerializationFeature.FAIL_ON_SELF_REFERENCES)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public static JsonUtils getInstance(){
        return Instance.instance;
    }

    public String asString(Object object){
        try {
            return objectMapper.writeValueAsString(object);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
