package com.muzuo.event;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.stereotype.Component;


/**
 * 异步消息处理失败
 * @ClassName: AsyncMsgExceptionHandler
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2024年3月11日
 *
 */
@Component
public class AsyncMsgExceptionHandler implements AsyncUncaughtExceptionHandler{
    
    private static Logger logger = LoggerFactory.getLogger(AsyncMsgExceptionHandler.class);
    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {
        try {
            logger.error("异步消息处理失败 方法名称: " + method +"参数:"+Arrays.toString(params), ex);
            //消息处理失败 发送通知
        } catch (Exception e) {
            
        }
    }
}
