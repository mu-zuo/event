package com.muzuo.event;
/**
 * 消息延时投递级别
 * @ClassName: DelayLevelType
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2021年1月12日
 *
 */
public enum DelayLevelType {
    /**
     * 延时1秒
     */
    Delay_1s(1),
    /**
     * 延时5秒
     */
    Delay_5s(2),
    /**
     * 延时10秒
     */
    Delay_10s(3),
    /**
     * 延时30s
     */
    Delay_30s(4),
    /**
     * 延时1分钟
     */
    Delay_1m(5),
    /**
     * 延时2分钟
     */
    Delay_2m(6),
    /**
     * 延时3分钟
     */
    Delay_3m(7),
    /**
     * 延时4分钟
     */
    Delay_4m(8),
    /**
     * 延时5分钟
     */
    Delay_5m(9),
    /**
     * 延时6分钟
     */
    Delay_6m(10),
    /**
     * 延时7分钟
     */
    Delay_7m(11),
    /**
     * 延迟8分钟
     */
    Delay_8m(12),
    /**
     * 延迟9分钟
     */
    Delay_9m(13),
    /**
     * 延迟10分钟
     */
    Delay_10m(14),
    /**
     * 延时20分钟
     */
    Delay_20m(15),
    /**
     * 延时30分钟
     */
    Delay_30m(16),
    /**
     * 延时1小时 
     */
    Delay_1h(17),
    /**
     * 延时2小时
     */
    Delay_2h(18);
    
    private Integer level;
    
    DelayLevelType(Integer level){
        this.level = level;
    }
    public Integer getLevel() {
        return level;
    }
    /**
     * 获取延时秒数
     * @Title: getLevelSeconds
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @param delayLevel
     * @param @return    参数
     * @return long    返回类型
     */
    public static long getLevelSeconds(DelayLevelType delayLevel) {
        switch (delayLevel) {
        case Delay_5s:
            return 5;
        case Delay_10s:
            return 10;
        case Delay_30s:
            return 30;
        case Delay_1m:
            return 60;
        case Delay_2m:
            return 120;
        case Delay_3m:
            return 180;
        case Delay_4m:
            return 240;
        case Delay_5m:
            return 300;
        case Delay_6m:
            return 360;
        case Delay_7m:
            return 420;
        case Delay_8m:
            return 480;
        case Delay_9m:
            return 540;
        case Delay_10m:
            return 600;
        case Delay_20m:
            return 1200;
        case Delay_30m:
            return 1800;
        case Delay_1h:
            return 3600;
        case Delay_2h:
            return 7200;
        default:
            return Delay_1s.getLevel();
        }
    }
}
