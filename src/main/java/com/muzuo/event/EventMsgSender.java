package com.muzuo.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * 消息发送者
 * @ClassName: EventMsgSender
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @date 2024年2月22日
 *
 */
@Component
public class EventMsgSender {
    private static final Logger logger = LoggerFactory.getLogger(EventMsgSender.class);
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    
    public void send(AbstractBaseMsg AbstractBaseEventMsg){
        eventPublisher.publishEvent(new BaseEvent<AbstractBaseMsg>(AbstractBaseEventMsg));
        logger.info("发送消息:{}", JsonUtils.getInstance().asString(AbstractBaseEventMsg));
    }
}
