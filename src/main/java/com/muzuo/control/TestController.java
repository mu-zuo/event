package com.muzuo.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.muzuo.event.CustomerRegister;
import com.muzuo.event.EventMsgSender;

@RestController
@RequestMapping(value = "/api/test")
public class TestController {
    @Autowired
    EventMsgSender sender;
    /**
     * 测试消息
     * @Title: testmsg
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param     参数
     * @return void    返回类型
     */
    @PostMapping("/testmsg")
    public void testmsg() {
        CustomerRegister test = CustomerRegister.build("我是消息内容 木左注册成功啦");
        sender.send(test);
    }

}